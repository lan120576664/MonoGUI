////////////////////////////////////////////////////////////////////////////////
// @file DlgShowEdit.cpp
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////
#include "../MonoGUI/MonoGUI.h"
#include "DlgShowEdit.h"

CDlgShowEdit::CDlgShowEdit()
{
}

CDlgShowEdit::~CDlgShowEdit()
{
}

// 消息处理过了，返回1，未处理返回0
int CDlgShowEdit::Proc (OWindow* pWnd, ULONGLONG nMsg, ULONGLONG wParam, ULONGLONG lParam)
{
	ODialog::Proc (pWnd, nMsg, wParam, lParam);

	if (pWnd = this)
	{
		if (nMsg == OM_NOTIFY_PARENT)
		{
			switch (wParam)
			{
			case 104:
				{
					OEdit* pEditStudent = (OEdit *)FindChildByID (102);
					OEdit* pEditSerial  = (OEdit *)FindChildByID (103);

					char student[LIST_TEXT_MAX_LENGTH];
					char serial[LIST_TEXT_MAX_LENGTH];
					memset (student, 0x0, sizeof(student));
					memset (serial, 0x0, sizeof(serial));

					pEditStudent->GetText(student);
					pEditSerial->GetText(serial);

					char info[LIST_TEXT_MAX_LENGTH * 2 + 100];
					sprintf (info, (char*)"姓名是：\n%s\n编号是：\n%s", student, serial);
					OMsgBox (this, (char*)"信息", info, OMB_INFORMATION | OMB_SOLID, 60);
				}
				break;

			case 105:
				{
					// 退出按钮
					O_MSG msg;
					msg.pWnd = this;
					msg.message = OM_CLOSE;
					msg.wParam = 0;
					msg.lParam = 0;
					m_pApp->PostMsg (&msg);
				}
				break;
			}
		}
	}

	return 1;
}

/* END */