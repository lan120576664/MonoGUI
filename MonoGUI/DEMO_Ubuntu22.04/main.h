////////////////////////////////////////////////////////////////////////////////
// @file main.h
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#if !defined(__MAIN_H__)
#define __MAIN_H__

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>
#include <pthread.h>

#define RUN_ENVIRONMENT_LINUX
#define MOUSE_SUPPORT
#define RESMGT_SUPPORT
#define CHINESE_SUPPORT

#define LCDW   SCREEN_W
#define LCDH   SCREEN_H

#define COLORBLACK	0x00000000
#define COLORWHITE	0xffffffff

#include "../MonoGUI/MonoGUI.h"
#include "DlgShowButtons.h"
#include "DlgShowEdit.h"
#include "DlgShowCombo.h"
#include "DlgShowList.h"
#include "DlgShowProgress.h"
#include "Desktop.h"
#include "MyApp.h"

#endif // !defined(__MAIN_H__)
