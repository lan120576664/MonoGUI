////////////////////////////////////////////////////////////////////////////////
// @file OMsgQueue.cpp
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#include "MonoGUI.h"

OMsgQueue::OMsgQueue ()
{
	m_nFront = 0;
	m_nRear  = 0;
}

OMsgQueue::~OMsgQueue ()
{
	// 清除所有消息
	RemoveAll ();
}

// 从消息队列中获取一条消息，取出的消息将被删除
// 如果遇到OM_QUIT消息，则返回0，消息队列空了返回-1，其他情况返回1；
int OMsgQueue::GetMsg (O_MSG* pMsg)
{
	if (m_nFront == m_nRear) {
		// 消息队列空
		return -1;
	}

	if (pMsg == NULL) {
		// 参数有问题
		return -1;
	}

	memcpy (pMsg, &(m_arMsgQ[m_nFront]), sizeof(O_MSG));

	// 删除已经得到的消息
	m_nFront = (m_nFront + 1) % MSG_QUEUE_SIZE;

	if (pMsg->message == OM_QUIT) {
		return 0;
	}

	return 1;
}

// 向消息队列中添加一条消息；
// 如果消息队列满（消息数量达到了MESSAGE_MAX 所定义的数目），则返回失败；
BOOL OMsgQueue::PostMsg (O_MSG* pMsg)
{
	// 消息队列满
	if ((m_nRear + 1) % MSG_QUEUE_SIZE == m_nFront) {
		return FALSE;
	}

	if (pMsg->message == OM_PAINT)
	{
		// 滤除重复的OM_PAINT消息
		if (FindMsg(pMsg)) {
			return FALSE;
		}
	}
	
	// 添加一条消息
	memcpy (&(m_arMsgQ[m_nRear]), pMsg, sizeof(O_MSG));
	m_nRear = (m_nRear + 1) % MSG_QUEUE_SIZE;

	return TRUE;
}

// 在消息队列中查找指定类型的消息；
// 如果发现消息队列中有指定类型的消息，则返回TRUE；
// 该函数主要用在定时器处理上。CheckTimer函数首先检查消息队列中有没有相同的定时器消息，如果没有，再插入新的定时器消息
BOOL OMsgQueue::FindMsg (O_MSG* pMsg)
{
	int count = (m_nRear - m_nFront + MSG_QUEUE_SIZE) % MSG_QUEUE_SIZE;
	int i;
	for (i = 0; i < count; i++)
	{
		if ((m_arMsgQ[i].pWnd    == pMsg->pWnd) &&
			(m_arMsgQ[i].message == pMsg->message) &&
			(m_arMsgQ[i].wParam  == pMsg->wParam) &&
			(m_arMsgQ[i].lParam  == pMsg->lParam))
		{
			return TRUE;
		}
	}
	return FALSE;
}

// 删除队列中的所有消息
BOOL OMsgQueue::RemoveAll ()
{
	m_nFront = 0;
	m_nRear  = 0;
	return TRUE;
}

/* END */