////////////////////////////////////////////////////////////////////////////////
// @file OScrollBar.h
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#if !defined (__OSCROLLBAR_H__)
#define __OSCROLLBAR_H__

#if defined (MOUSE_SUPPORT)
// 定义鼠标左键落点的测试结果
enum
{
	SCROLL_PT_UP       = 1,
	SCROLL_PT_PAGEUP   = 2,
	SCROLL_PT_DOWN     = 3,
	SCROLL_PT_PAGEDOWN = 4,
	SCROLL_PT_BUTTON   = 5
};
#endif // defined(MOUSE_SUPPORT)

class OScrollBar
{
private:
	int m_nRange;		// 移动范围
	int m_nSize;		// 中间按钮的大小
	int m_nPos;			// 当前位置
/* 例如：
 * 一个列表框有40条列表项，每页显示6个，当前最上面一条是第25条，则：
 * m_nRange = 40;    m_nSize = 6;    m_nPos = 24;
 */

#if defined (MOUSE_SUPPORT)
	int m_nOldPos;      // 记录鼠标点中按钮时的位置
	int m_nOldPt;       // 记录鼠标点中按钮时的坐标
						// (垂直滚动条记录y坐标，水平滚动条记录x坐标)
#endif // defined(MOUSE_SUPPORT)

public:
	int m_nStatus;		// 0：不显示；1：显示
	int m_nMode;		// 1：垂直滚动条；2：水平滚动条
	int m_x;
	int m_y;
	int m_w;
	int m_h;			// 大小位置(滚动条的宽度强制设为13)

public:
	OScrollBar ();
	virtual ~OScrollBar ();

	// 创建滚动条（应指定水平还是垂直）
	BOOL Create (int nMode, int x, int y, int w, int h, int nRange, int nSize, int nPos);

	// 绘制滚动条
	void Paint (LCD* pLCD);

	// 设置滚动范围
	BOOL SetRange (int nRange);

	// 设置中间按钮的大小
	BOOL SetSize (int nSize);

	// 设置当前位置
	BOOL SetPos (int nPos);

#if defined (MOUSE_SUPPORT)
	// 记录当前位置和当前坐标
	BOOL RecordPos (int nPt);

	// 判断鼠标落点
	int TestPt (int x, int y);

	// 根据鼠标坐标计算对应的新位置
	int TestNewPos (int x, int y);
#endif // defined(MOUSE_SUPPORT)
};

#endif // !defined(__OSCROLLBAR_H__)
