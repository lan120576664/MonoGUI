////////////////////////////////////////////////////////////////////////////////
// @file OCaret.h
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#if !defined(__OCARET_H__)
#define __OCARET_H__

typedef struct _CARET
{
	BOOL	bValid;				// 是否使用脱字符
	int	x;						// 位置
	int	y;
	int	w;						// 宽高
	int	h;
	BOOL	bFlash;				// 是否闪烁
	BOOL	bShow;				// 标志当前状态为显示或者隐藏
	ULONGLONG	lTimeInterval;	// 闪烁的时间间隔（一般采用500毫秒）
} CARET;

class OCaret
{
public:
	CARET m_Caret;
	ULONGLONG m_lLastTime;

public:
	OCaret();
	virtual ~OCaret();

	// 根据窗口的脱字符信息设置系统脱字符的参数；
	BOOL SetCaret (CARET* pCaret);

	// 更新脱字符的显示。
	// 如果脱字符定时器到时了，则将主缓中脱字符区域的图像以适当方式送入FrameBuffer的对应位置；
	BOOL Check (LCD* pLCD, LCD* pBuf);

	// 绘制脱字符
	void DrawCaret (LCD* pLCD, LCD* pBuf);
};

#endif // !defined(__OCARET_H__)
